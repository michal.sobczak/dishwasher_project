#include <iostream>
#include <fstream>
#include <cstdlib>
#include <vector>
#include<string>
#include<conio.h>
#include <windows.h>

using namespace std;
class programs
{
	public:
	int id;
	string name;
	int time;
	int water;
	int energy;
	
	programs( int xid, string xname, int xtime, int xwater, int xenergy)
		{
			id=xid;
			name=xname;
			time = xtime;
			water = xwater;
			energy = xenergy;
		}
	programs()
	{
		
	};
	void print_data()
		{
			cout<<id<<". "<<name<<" -  time: "<<time<<" - water:"<<water<<" energy: "<<energy<<endl;
		}
		
	void print_name()
		{
			cout<<"- "<<name<<endl;
		}
};

class last_programs : public programs
{
	public:
		
	last_programs( int xid, string xname, int xtime, int xwater, int xenergy)
	{
			id=xid;
			name=xname;
			time = xtime;
			water = xwater;
			energy = xenergy;
	}
	last_programs()
	{
		
	};
	
	
	void change_last_program(vector<programs> & tab_programs, int program_number)
	{
		id=tab_programs[program_number-1].id;
		name=tab_programs[program_number-1].name;
		time = tab_programs[program_number-1].time;
		water = tab_programs[program_number-1].water;
		energy = tab_programs[program_number-1].energy;	
	}
	
	void save_last_program()
{
	fstream last_program_file;
	
	last_program_file.open("last_program.txt", ios::out);
	
	last_program_file<<id;
	
	last_program_file.close();
}
};
class current_program
{
	public:
		int program_number;
		int time_left;
		
		current_program( int xprogram_number, int xtime_passed)
		{
			program_number = xprogram_number;
			time_left = xtime_passed;
		}
		
	void print_data()
	{
		cout<<"program number: "<< program_number<<"time left: "<<time_left<<endl;
	}
	
	void set_program(int n, programs p)
	{
		program_number = n;
		time_left = p.time;
	}
	
	
	/**
  * Gets: number of seconds to wait for user input
  * Returns: '_' if there was no input, otherwise returns the char inputed
**/
	char start_washing()
	{
	
    char c = '_'; //default return
    cout<<"press p for pause, s for stop"<<endl;
    while( time_left != 0 ) {
        if( _kbhit() ) { //if there is a key in keyboard buffer
            c = _getch(); //get the char
            break; //we got char! No need to wait anymore...
        }

        Sleep(1000); //one second sleep
        --time_left; //countdown a second
        cout<<".";
    }
    cout<<endl<<c<<endl;
    if (time_left == 0) program_number = 0;
    return c;
}
};


class program_operator
{

	public:
				program_operator()
	{
		
	};
		void read_programs(vector<programs> &tab_programs)
		{
	
			fstream programs_file;
			int xid;
			string name;
			int time;
			int water;
			int energy;
	
			programs_file.open("programs.txt", ios::in);
		
			for( int i = 0; i<4;i++)
			{
				programs_file>>xid;
				programs_file>>name;
				programs_file>>time;
				programs_file>>water;
				programs_file>>energy;

	

				tab_programs.push_back( programs (xid, name, time, water, energy));

			}
			programs_file.close();
		};

		void read_current_program(current_program &cp)
		{
			fstream current_program_file;
			int time_passed;
			int program_number;
	
			current_program_file.open("current_program.txt", ios::in);
	
			current_program_file>>program_number;
			current_program_file>>time_passed;
	
			cp = current_program(program_number, time_passed);
	
			current_program_file.close();
		}

		void read_last_program(programs &last_program ,vector<programs> & tab_programs)
		{
			int program_number;
			fstream last_program_file;
	
			last_program_file.open("last_program.txt", ios::in);
	
			last_program_file>>program_number;
	
			last_program_file.close();
	
			last_program.id=tab_programs[program_number-1].id;
			last_program.name=tab_programs[program_number-1].name;
			last_program.time = tab_programs[program_number-1].time;
			last_program.water = tab_programs[program_number-1].water;
			last_program.energy = tab_programs[program_number-1].energy;
	
			cout<<endl<<program_number<<endl;
		}
		void save_current_program(current_program &cp)
		{
			fstream current_program_file;
			current_program_file.open("current_program.txt", ios::out);
	
			current_program_file<<cp.program_number<<" "<<cp.time_left;
	
			current_program_file.close();
		}
		void print_programs(vector<programs> & tab_programs)
		{
			for( int i = 0; i<tab_programs.size(); i++)
			{
				tab_programs[i].print_data();
			}
		}
		
};


	
main () {
	int n;
	char c;
	char p;
	vector<programs> tab_programs;	
	current_program cp(current_program(0,0));
	last_programs last_program;
	program_operator program_operator1;

	program_operator1.read_programs(tab_programs);	
	program_operator1.read_current_program(cp);
	program_operator1.read_last_program(last_program, tab_programs);
	
while(1){
	system("cls");
	cout<<"hello, welcome in dishwasher1000"<<endl;
	cout<<"last used program: ";
	last_program.print_name();
	if (cp.program_number != 0)
	{
		cout<<"finishing last program"<<endl;		
		tab_programs[cp.program_number-1].print_data();
		cp.print_data();
		c = cp.start_washing();
	}
	else
	{
		cout<<"avalible programs: "<<endl;
		program_operator1.print_programs(tab_programs);
		cout<<"chose program number:"<<endl;
		cin>>n;
		cout<<"chosen program: ";
		tab_programs[n-1].print_data();
		cp.set_program(n, tab_programs[n-1]);
		last_program.change_last_program(tab_programs, n);
		last_program.save_last_program();
		
		
		cout<<"program started"<<endl;
		c = cp.start_washing();
		
			
		while(c=='p')
		{
			cout<<"cycle paused. Press:"<<endl<<"p to resume"<<endl<<"s to stop"<<endl<<"t to turn off"<<endl;
			cin>>c;
			if (c=='p')
			{
				cout<<"program resumed"<<endl;
				c = cp.start_washing();
				cout<<c<<endl;
			}
			if(c=='t') 
			{
			program_operator1.save_current_program(cp);
			return 0;
			}
		}
		
		
		if (c=='s')
		{
			cout<<"program stopped";
			Sleep(1000);
			cp.time_left = 0;
			cp.program_number = 0;
			
		}		
		
	}
	
	
	
		
	program_operator1.save_current_program(cp);
}
	
	
return 0;
}

